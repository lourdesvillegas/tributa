<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Asesoría Tributaria</h2>
</section>

<!-- Contactanos-->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ">
                    
                    <p class="text-justify">Siendo conocedores que el sistema tributario de nuestro país es complejo, nuestro estudio nace bajo la concepción de ofrecer una completa asesoría en materia fiscal a los clientes, con la finalidad de facilitar la toma de sus decisiones de manera acertiva, habiendo para ello diseñado diversos productos, tales como: Consultas tributarias y estudios especiales, reclamación, apelación y defensa judicial, planeamiento tributario, absolución de esquelas y cartas inductivas, acompañamiento legal en fiscalizaciones, entre otros servicios.</p>
            
            <!--<div style="background-image: url(img/service7.jpg);" class="imagen"></div>-->
                </div>

                <div class="col-lg-6 ">
                    <div class=" text-center">
                        <i class="fa fa-check fa-4x" aria-hidden="true"></i>

                        <hr class="separator">
                        <a class="btn boton-firma" href="#" role="button">Solicitar Servicio</a>
                    </div>
                </div>
            </div>
        </div>
        
    </section>


<?php
include ('sub-footer.php');
include('footer.php');
?>