<?php
include ('head.php');
include('header.php');
?>
<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Nuestro Estudio</h2>
</section>

<!-- -->
    <section class="body-int">
        <div class="container">
	<div class="">
        <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center" >
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i> <br/>Servicios Legales Corporativos
                </a>
                <a href="#" class="list-group-item text-center" >
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Asesoría Permanente
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Liberación de Fondos de Detracciones e Impugnación de Ingreso como Recaudación
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Fraccionamientos
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Gestión en Procedimientos de Cobranza Coactiva
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Planeamiento Tributario
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Auditoría Tributaria Preventiva
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Recuperación de Impuestos a los Exportadores
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Devoluciones
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Soporte en Fiscalizaciones
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Absolución de Esquelas y Cartas Inductivas
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Reclamación, Apelación y Defensa Judicial
                </a>
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Consultas Tributarias y Estudios Especiales
                </a>

              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active" >
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Servicios Legales Corporativos</h2>
                      <p class="text-justify">Adicionalmente, a nuestros servicios en tributación, brindamos consultoría complementaria en Derecho Administrativo, Contractual, Mercantil y Laboral con el objeto de garantizar que nuestros clientes cuenten con una asesoría integral sobre el marco normativo que regula las actividades de la empresa.</p>
                      <img src="img/service1.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content"  >
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Asesoría Permanente</h2>
                      <p class="text-justify">Es un servicio especial que ofrece diversos productos tributarios que buscan facilitar el correcto desarrollo de las actividades empresariales, y que incluye la atención de consultas tributarias, asistencia en trámites de RUC, comprobantes de pago y devoluciones, asesoría en la obtención de fraccionamientos y apoyo en los procedimientos de cobranza coactiva.</p>
                      <img src="img/service6.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
    
                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Liberación de Fondos de Detracciones e Impugnación de Ingreso como Recaudación</h2>
                      <p class="text-justify">Asesoramos en el trámite para obtener la liberación de los fondos depositados en la cuentas del Banco de la Nación abiertas en aplicación del Sistema de Pago de Obligaciones Tributarias con el Gobierno Central (Detracciones).</p>
                       <p class="text-justify">
						Asimismo, patrocinamos a nuestros clientes en la presentación de recursos administrativos (reconsideración, apelación y revisión) y en la vía judicial, cuando la autoridad tributaria haya denegado la liberación de fondos o dispuesto su ingreso como recaudación vulnerando los derechos de los contribuyentes.</p>
                      <img src="img/service6.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Fraccionamientos</h2>
                      <p class="text-justify">Ofrecemos a nuestros clientes la verificación del estado de su deuda tributaria con el propósito de asegurar un exitoso acogimiento a los beneficios tributarios de caráter general o particular que se encuentren vigentes.</p>
						<p class="text-justify">Adicionalmente, apoyamos en el cumplimiento del cronograma de pago de sus cuotas de fraccionamiento, alertándolos con anticipación a su vencimiento, evitando con ello que se produzca la pérdida del beneficio.</p>
                      <img src="img/service6.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                   <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Gestión en Procedimientos de Cobranza Coactiva</h2>
                      <p class="text-justify">Brindamos asesoría legal durante el procedimiento de cobranza coactiva a fin de asegurar que su desarrollo se efectúe de acuerdo a ley.
						Del mismo modo, buscamos soluciones adecuadas para el pago de la deuda tributaria exigible procurando que, en lo posible, no afecten financieramente a nuestros clientes.</p>
						<p class="text-justify">
						Asistimos en intervenciones excluyentes de propiedad y en recursos de queja ante el Tribunal Fiscal cuando la autoridad tributaria haya infringido el procedimiento de cobranza coactiva.</p>
                      <img src="img/service9.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Planeamiento Tributario</h2>
                      <p class="text-justify">Facilitamos la toma de decisiones empresariales proporcionando a nuestros clientes, el conocimiento de las consecuencias tributarias de las operaciones que se pretendan implementar.</p>
                      	<p class="text-justify">
						Nuestro servicio comprende la recomendación de las alternativas previstas en la legislación tributaria que permitan obtener los mayores beneficios para la empresa minimizando costos impositivos.</p>
                      <img src="img/service13.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Auditoría Tributaria Preventiva</h2>
                      <p class="text-justify">Este servicio tiene el propósito de minimizar las contingencias tributarias, verificando antes de cualquier intervención de la Administración Tributaria, el correcto cumplimiento de las obligaciones de la empresa, a fin de subsanar las omisiones o infracciones que se pudieran estar presentando, labor que comprende entre otras acciones:</p>
                      <ul>
                      	<li>Revisión de los aspectos formales referidos a los comprobantes de pago, los libros de contabilidad y la presentación de las declaraciones pago.</li>
                      	<li>Confrontación de la correspondencia entre las operaciones anotadas en los libros contables y lo consignado en las declaraciones pago.</li>
                      	<li>Evaluación del correcto tratamiento tanto de las ventas e ingresos así como de los gastos y egresos.</li>
                      	<li>Examen del correcto cumplimiento de las normas tributarias referidas a la determinación de los tributos.</li>
                      </ul>
                      <img src="img/service14.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Recuperación de Impuestos a los Exportadores</h2>
                      <p class="text-justify">Efectuamos los análisis previos que van a sustentar las solicitudes de devolución del Saldo a Favor del Exportador que serán presentadas ante la Administración Tributaria, asegurando con ello la recuperación de los montos que correspondan conforme a la ley.</p>
                      <img src="img/service8.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Devoluciones</h2>
                      <p class="text-justify">Ofrecemos el estudio, la preparación y en general todo el apoyo necesario para la presentación de las solicitudes de devolución de impuestos que requieran nuestros clientes, tales como pagos indebidos o en exceso, percepciones, retenciones, entre otros.</p>
                      <img src="img/service2.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Soporte en Fiscalizaciones</h2>
                      <p class="text-justify">data
						Apoyamos a nuestros clientes en los procesos de fiscalización iniciados por la autoridad tributaria, orientándolos en la forma de absolver los distintos requerimientos y solicitudes de información, a fin de levantar las observaciones y evitar futuras determinaciones de impuestos.</p>
                      <img src="img/service11.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Absolución de Esquelas y Cartas Inductivas</h2>
                      <p class="text-justify">Prestamos asistencia legal y/o contable a fin de preparar sólidos argumentos que sustenten las respuestas a las esquelas y cartas inductivas que permanentemente notifica de manera masiva la Administración Tributaria.</p>
						<p class="text-justify">Las respuestas que se ofrezcan permitirán evitar una fiscalización o, en todo caso, afrontar la auditoría de manera ordenada y exitosa.</p>
                      <img src="img/service4.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Reclamación, Apelación y Defensa Judicial</h2>
                      <p class="text-justify">Diseñamos  una  adecuada  estrategia  de  defensa  sobre la base del estudio de las normas legales, jurisprudencia fiscal y de los papeles de trabajo preparados por el área de fiscalización de la SUNAT, a fin de obtener los mejores resultados  para  nuestros  clientes  en  los  procedimientos  de  reclamación y apelación, así como en los procesos judiciales relacionados con temas tributarios.</p>
                      <img src="img/service7.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <img src="img/book.png" class=" img-responsive " alt="" >
                      <h2 >Consultas Tributarias y Estudios Especiales</h2>
                      <p class="text-justify">Atendemos  cualquier  problemática  de  carácter  tributario  mediante  la  elaboración  de   informes técnicos que permitan establecer el sentido y alcance de las normas tributarias.</p>
						<p class="text-justify">Asimismo, ofrecemos estudios especiales referidos a tributación sectorial e internacional, brindando información dentro del marco normativo vigente.</p>
                      <img src="img/service12.jpg" class=" img-responsive " alt="" >
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
        
    </section>

<?php
include ('sub-footer.php');
include('footer.php');
?>