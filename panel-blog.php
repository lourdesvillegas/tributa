<div class="do-blog-sidebar-widget blog-panel">
    <h3 class="do-sidebar-widget-title">CATEGORIAS</h3>

    <ul>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum   </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum       </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum        </a>
        </li>
    </ul>
</div>

<div class="do-blog-sidebar-widget blog-panel">
	                    <h3 class="do-sidebar-widget-title">Suscríbete al Blog</h3>
	                    <form action="" id="newsletter-form" novalidate="novalidate">
	                        <div class="row">
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <input id="nombre" name="nombre" type="text" class="form-control" placeholder="Nombre">
	                                </div> 
	                            </div>
	                            <div class="col-md-6">
	                                <div class="form-group">
	                                    <input id="apellido" name="apellido" type="text" class="form-control" placeholder="Apellido">
	                                </div> 
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="form-group">
	                                   <input id="email" name="email" type="email" class="form-control" placeholder="Correo"> 
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="form-group">
	                                    <div class="checkbox">
	                                        <label>
	                                            <input type="checkbox" id="terminos" name="terminos"> Acepto los <a href="#" data-toggle="modal" data-target="#modalterminos">términos y condiciones de uso</a> 
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div style="text-align: center;">
	                            <button type="submit" class="btn boton-firma">Enviar</button>
	                        </div>
	                    </form>
	                </div>