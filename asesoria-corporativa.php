<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Asesoría Corporativa</h2>
</section>

<!-- -->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ">
                    
                    <p class="text-justify">Adicionalmente, a nuestros servicios especializados en tributación, brindamos consultoría complementaria en Derecho Comercial, lo que comprende constitución de empresas, elaboración de todo tipo de contratos y liquidación de empresas conforme a la ley de la materia y vía declaración de quiebra judicial. Asimismo, nos encargamos de ejercer labores de representación de empresas no domiciliadas en el país y la constitución de sucursales extranjeras.</p>
                             
                </div>

                <div class="col-lg-6 ">
                    <div class=" text-center">
                        <i class="fa fa-check fa-4x" aria-hidden="true"></i>
                        <hr class="separator">
                        <a class="btn boton-firma" href="#" role="button">Solicitar Servicio</a>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

<?php
include ('sub-footer.php');
include('footer.php');
?>