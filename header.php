<body >
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-page-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>                
                <a class="navbar-brand" href="#page-top">
                    <img src="img/logo.png" class="img-responsive" alt="" >
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-page-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="">Nuestra </br>Firma</a>
                    </li>
                    <li class="">
                        <a href="">Nuestro </br>Estudio</a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nuestros </br> Colaboradores <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Socios</a></li>
                        <li><a href="#">Asociados</a></li>
                        <li><a href="#">Consultores</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Áreas </br>de Trabajo <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Asesoría Tributaria</a></li>
                        <li><a href="#">Asesoría Laboral</a></li>
                        <li><a href="#">Asesoría Corporativa</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios </br>Complementarios <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Outsourcing Contable</a></li>
                        <li><a href="#">Facturación Electronica</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><br>Novedadeds <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Boletín Legal</a></li>
                        <li><a href="#">Publicaciones</a></li>
                        <li><a href="#">Noticias</a></li>
                        <li><a href="#">Brochure</a></li>
                      </ul>
                    </li>
                    <li class="">
                        <a href=""><br>Contáctenos</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>