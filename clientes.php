<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Nuestros Clientes</h2>
</section>


<!-- clientes-->
    <section class="body-int">
        <div class="container">
            <div class="row">
            
            <div class="col-lg-3 col-md-6 col-xs-12 ">
                <div class="team-members">
                    <img class=" img-responsive"  src="img/clientes/client-agroindustrial.jpg" alt="">
                    <div class="team-member-details">
                        <h2>CITE AGROINDUSTRIAL</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 ">
                <div class="team-members">
                    <img class=" img-responsive"  src="img/clientes/client-alimas.jpg" alt="">
                    <div class="team-member-details">
                        <h2>ALIMAS S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 ">
                <div class="team-members">
                    <img class=" img-responsive"src="img/clientes/client-aprenda.jpg" alt="">
                    <div class="team-member-details">
                        <h2>APRENDA S.A.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 ">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-arquimagen.jpg" alt="">
                    <div class="team-member-details">
                        <h2>ARQUIMAGEN S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-capeco.jpg" alt="">
                    <div class="team-member-details">
                        <h2>INSTITUTO CAPECO</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-capunay.jpg" alt="">
                    <div class="team-member-details">
                        <h2>CAPUÑAY Y CIEZA ABOGADOS</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-default.jpg" alt="">
                    <div class="team-member-details">
                        <h2>C &amp; C ABOGADOS SAC</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-default.jpg" alt="">
                    <div class="team-member-details">
                        <h2>CONDESO COLLAO SANDRA LESLIE</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-coviem.jpg" alt="">
                    <div class="team-member-details">
                        <h2>COVIEM S.A</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-dondewalter.jpg" alt="">
                    <div class="team-member-details">
                        <h2>DONDE WALTER S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-eci.jpg" alt="">
                    <div class="team-member-details">
                        <h2>ENERGIA Y CONTROLES INDUSTRIALES E.I.R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-electro.jpg" alt="">
                    <div class="team-member-details">
                        <h2>ELECTRICIDAD DEL PERU ELECTROPERU S A</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-erumix.jpg" alt="">
                    <div class="team-member-details">
                        <h2>CONCRETO PERUMIX SRL</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-farenet.jpg" alt="">
                    <div class="team-member-details">
                        <h2>FARENET</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-fice.jpg" alt="">
                    <div class="team-member-details">
                        <h2>FICE CONSULTORES SAC</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-finver.jpg" alt="">
                    <div class="team-member-details">
                        <h2>FINVER CALLAO</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-geriatrics.jpg" alt="">
                    <div class="team-member-details">
                        <h2>GERIMED S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-gomez.jpg" alt="">
                    <div class="team-member-details">
                        <h2>GOMEZ MUEBLES &amp; PROYECTOS S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-ingenia.jpg" alt="">
                    <div class="team-member-details">
                        <h2>INGENIA ASOCIADOS S.R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-itp.jpg" alt="">
                    <div class="team-member-details">
                        <h2>INSTITUTO TECNOLOGICO DE LA PRODUCCION</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-lequiperu.jpg" alt="">
                    <div class="team-member-details">
                        <h2>LEQUI PERU S.A.C</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-lurin.jpg" alt="">
                    <div class="team-member-details">
                        <h2>MUNICIPALIDAD DE LURIN</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-mardigital.jpg" alt="">
                    <div class="team-member-details">
                        <h2>MAR DIGITAL</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-moreno.jpg" alt="">
                    <div class="team-member-details">
                        <h2>MORENO &amp; PEREZ ABOGADOS Y CONTADORES S. CIVIL DE R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-mtb.jpg" alt="">
                    <div class="team-member-details">
                        <h2>MTB TECNOLOGIA E.I.R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-nering.jpg" alt="">
                    <div class="team-member-details">
                        <h2>NERIO NORIEGA INGENIEROS SA</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-osiner.jpg" alt="">
                    <div class="team-member-details">
                        <h2>ORGANISMO SUPERVISOR DE LA INVERSION EN ENERGIA Y MINERIA</h2>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-proveedores.jpg" alt="">
                    <div class="team-member-details">
                        <h2>PROVEEDORES MINEROS S.A.C.</h2>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-purifisa.jpg" alt="">
                    <div class="team-member-details">
                        <h2>PURIFISAGLOBAL PERU S.R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-pastipan.jpg" alt="">
                    <div class="team-member-details">
                        <h2>PASTIPAN SOCIEDAD ANONIMA CERRADA</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-sammy.jpg" alt="">
                    <div class="team-member-details">
                        <h2>SAMMYTEC S.A.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-senor.jpg" alt="">
                    <div class="team-member-details">
                        <h2>SR DE LOS MILAGROS CARGO E.I.R.L.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-serminas.jpg" alt="">
                    <div class="team-member-details">
                        <h2>SERMINAS S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-softline.jpg" alt="">
                    <div class="team-member-details">
                        <h2>SOFTLINE INTERNATIONAL PERU S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-soprin.jpg" alt="">
                    <div class="team-member-details">
                        <h2>SOPRIN S.A.C.</h2>
                    </div>
                </div>
            </div>
            

            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-taxivip.jpg" alt="">
                    <div class="team-member-details">
                        <h2>VIP CARS TRANSPORT S.A.C.</h2>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-transcar.jpg" alt="">
                    <div class="team-member-details">
                        <h2>AUTOMOTRIZ TRANS CAR S.A.C.</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-u.jpg" alt="">
                    <div class="team-member-details">
                        <h2>CLUB UNIVERSITARIO DE DEPORTES</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <div class="team-members">
                    <img class=" img-responsive" src="img/clientes/client-universidad.jpg" alt="">
                    <div class="team-member-details">
                        <h2>UNIVERSIDAD INCA GARCILASO DE LA VEGA</h2>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-lg-12 col-mc-12 col-sm-12 col-sx-12">
                
                <h1 class="" style="margin-bottom: 30px;">Otros Clientes</h1>
            </div>
            <div class="col-lg-12 col-mc-12 col-sm-12 col-sx-12">
                <ul class="list-group">
                    <li class="list-group-item">ALIANZA DE SERVICIOS E.I.R.L.</li>
                    <li class="list-group-item">AUDICONT PERU S.A.C.</li>
                    <li class="list-group-item">EMPRESA CONSTRUCTORA A.R.C S.R.LTDA</li>
                    <li class="list-group-item">ESTRATEGIA ASOCIADOS S.R.L.</li>
                    <li class="list-group-item">FREAK CONSTRUCTORES Y CONSULTORES S.R.L.</li>
                    <li class="list-group-item">GRUPO TECNOLOGICO AUTOMOTRIZ S.A.C.</li>
                    <li class="list-group-item">GURMENDI MINERIA Y CONSTRUCCION S.A.C.</li>
                    <li class="list-group-item">KENSINGTON SAC</li>
                    <li class="list-group-item">PROGRAMMING C.A.R.SOCIEDAD ANONIMA CERRADA</li>
                    <li class="list-group-item">PROMOGEST SOCIEDAD ANONIMA CERRADA</li>
                    <li class="list-group-item">REMIMEDICAL S.R.LTDA.</li>
                    <li class="list-group-item">RESTAURANT ITALO S.A.C.</li>
                </ul>
            </div>
        
        </div>
    </section>

<?php
include ('sub-footer.php');
include('footer.php');
?>