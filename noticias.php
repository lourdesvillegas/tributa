<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Noticias</h2>
</section>

<!-- -->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 space">
                    
                    <div class="">

                          <div class="col-md-6">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="img/service7.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Para tener una buena relación con los demás, primero debes tenerla contigo                          </h3>
                                          <p >
                                              Lo que vemos en otro es un reflejo nuestro. Este reflejo también se ve en nuestras relaciones.      </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>   
                            <div class="col-md-6">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="img/service7.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Silencio interno y conexión con uno mismo                          </h3>
                                          <p >
                                              Una forma de lograr desarrollar nuestro lado espiritual es darnos un tiempo para hacer silencio, tanto a nivel del habla como de la mente     </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="img/service7.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Silencio interno y conexión con uno mismo                          </h3>
                                          <p >
                                              Una forma de lograr desarrollar nuestro lado espiritual es darnos un tiempo para hacer silencio, tanto a nivel del habla como de la mente     </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>  

                            <div class="col-md-6">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="img/service7.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Silencio interno y conexión con uno mismo                          </h3>
                                          <p >
                                              Una forma de lograr desarrollar nuestro lado espiritual es darnos un tiempo para hacer silencio, tanto a nivel del habla como de la mente     </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>    

                    </div>
                             
                </div>

                <div class="col-lg-4 ">
                  <?php

                  include('panel-blog.php');
                  ?>
                    
                </div>
            </div>
        </div>
        
    </section>

<?php

include('footer.php');
?>