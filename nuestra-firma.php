<?php

include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Nuestra firma</h2>

	<!--<div style="background-image: url(img/plant-image.jpg);">
     </div>-->
</section>

<section class="body-int">
	<div class="container">
        <div class="row">
            <div class="col-lg-6 conten-firma">
            	<p>Somos profesionales especializados en materia tributaria con muchos años de experiencia en el rubro, dispuesto a satisfacer las necesidades de los clientes con la mayor transparencia y confidencialidad. Nuestra firma tiene la satisfacción de haber colaborado eficazmente para que nuestros clientes tomen decisiones oportunas y, del mismo modo, haberlos asesorado a resolver problemas concretos que dificultaban su marcha empresarial.</p>
            	<div class="space"></div>
            	<img src="img/plant-image.jpg" class="img-responsive">
            </div>
            <div class="col-lg-6">
            	<div class="solution conten-firma">
            		<div class="icon">
            			<i class="fa-4x fa fa-check" aria-hidden="true" ></i>
            		</div>
            		<div class="solution-text">
            			<h3>CONOCIMIENTO</h3>
            			<p>Los profesionales que integran nuestro Estudio han servido en las diversas áreas de SUNAT por más de 20 años, lo cual les proporciona el conocimiento y la experiencia necesaria por salvaguardar de manera satisfactoria los intereses de los clientes.</p>
            		</div>
            	</div>


            	<div class="solution conten-firma">
            		<div class="icon">
            			<i class="fa-4x fa fa-check" aria-hidden="true" ></i>
            		</div>
            		<div class="solution-text">
            			<h3>DEDICACIÓN Y ATENCIÓN PERSONALIZADA</h3>
            			<p>El Estudio por vocación se involucra en los casos que atiende, ofreciendo un servicio diferente e individualizado, permitiéndonos tener un mejor conocimiento de la realidad de cada cliente, conllevando a obtener mejores resultados. Nos encargamos de escuchar sus necesidades y de darle una solución pronta y oportuna</p>
            		</div>
            	</div>

            	<div class="solution conten-firma">
            		<div class="icon">
            			<i class="fa-4x fa fa-check" aria-hidden="true" ></i>
            		</div>
            		<div class="solution-text">
            			<h3>COSTOS RAZONABLES</h3>
            			<p>Una ventaja comparativa en relación a otros estudios de abogados, es que nuestros servicios mantienen costos por debajo de los precios que se ofrecen en el mercado.</p>
            		</div>
            	</div>


            </div>
         </div>
    </div>

</section>

<?php
include('sub-footer.php');
include('footer.php');
?>
