<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Asesoría Laboral</h2>
</section>

<!-- Contactanos-->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ">
                    
                    <p class="text-justify">Considerando que la constante actualización en materia tributaria es un requisito indispensable que deben tener presente los directivos de las empresas, nuestro estudio brinda capacitaciones In House y asimismo, organiza seminarios para el público en general. Estos cursos garantizan que el cliente y/o asistente reciba el conocimiento y las habilidades necesarias para aplicarlas en el desarrollo de sus actividades comerciales y/o profesionales.</p>
                    <div class="space"></div>

                    <h4>AUDITORÍA PREVENTIVA TRIBUTARIA</h4>

                    <p class="text-justify">Buscamos minimizar las contingencias tributarias, verificando antes de cualquier intervención de la autoridad tributaria, el correcto cumplimiento de las obligaciones de nuestros clientes, a fin de subsanar las omisiones o infracciones que se pudieran estar presentando.</p>          
                </div>

                <div class="col-lg-6 ">
                    <div class=" text-center">
                        <i class="fa fa-check fa-4x" aria-hidden="true"></i>
                        <hr class="separator">
                        <a class="btn boton-firma" href="#" role="button">Solicitar Servicio</a>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

<?php
include ('sub-footer.php');
include('footer.php');
?>