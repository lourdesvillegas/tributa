<div class="do-blog-sidebar-widget blog-panel">
    <h3 class="do-sidebar-widget-title">CATEGORIAS</h3>

    <ul>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum   </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum       </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum        </a>
        </li>
    </ul>
</div>

<div class="do-blog-sidebar-widget blog-panel">
    <h3 class="do-sidebar-widget-title">POST RECIENTES</h3>

    <ul>
    	<li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum    </a>
        </li>
    	<li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum  </a>
        </li>
    	<li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum      </a>
        </li>
    	<li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum      </a>
        </li>
    	<li>
            <a href="#">
                <i class="fa fa-angle-right"></i>Lorem Ipsum          </a>
        </li>
    </ul>
</div>

<div class="do-blog-sidebar-widget do-tag-cloud blog-panel">
    <h3 class="do-sidebar-widget-title">ETIQUETAS</h3>

    <div class="do-tagcloud-wrapper clearfix">
        
         <a href="#">lorem</a> <a href="#">ipsum</a>
                                
    </div>
</div>