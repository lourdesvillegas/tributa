<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Nuestros Asociados</h2>
</section>


<section class="body-int">
	<div class="container">
        <div class="row">
            <div class="col-lg-6 ">

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Perfil</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Especialización</a></li>
				    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Experiencia Profesional</a></li>
				    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Membresía</a></li>
				    <li role="presentation"><a href="#univer" aria-controls="univer" role="tab" data-toggle="tab">Enseñanza Universitaria</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="home">
				    	<div class="space"></div>
				    	 <center><img src="img/team2a.jpg" class="img-responsive"></center>
				    	<div class="team-member-details">
                          <center>
                          	<h2>Alfredo A. Carella Feliziani</h2>
                          	<p>Abogado por la Pontificia Universidad Católica del Perú</p>
                          </center>
                            <div class="team-member-socials">
                            	<center>
                                <a target="_blank" href="https://www.facebook.com/Tributa-Consultores-150700788429315/?fref=ts"><i class="fa team-member-icons fa-facebook"></i></a>
                                 <a target="_blank" href="#"><i class="fa team-member-icons fa-linkedin"></i></a></center>
                            </div>
                        </div>
            			
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="profile">
				    	<ul>
				    		<li>Curso de Administración Tributaria (CAT XV) en el Instituto de Administración Tributaria (IAT) de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Curso de Contabilidad Básica en el Instituto de Administración Tributaria (IAT) de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Curso de Código Tributario en el Instituto de Administración Tributaria (IAT) de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Desarrollo de Habilidades Directivas por la Universidad del Pacifico.</li>
				    		<li>Especialización en Gestión Pública por la Universidad del Pacífico.</li>
				    		<li>Curso de Formación de Árbitros del Colegio de Abogados de Lima – CAL.</li>
				    		<li>Especialización en Derecho Tributario por la Pontificia Universidad Católica del Perú.</li>
				    	</ul>
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="messages">
				    	<ul>
				    		<li>Funcionario de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT (1993-2013).</li>
				    		<li>Profesional Orientador Tributario en la División de Atención al Contribuyente de la Intendencia Regional Ica (1994-1997).</li>
				    		<li>Profesional Orientador Tributario en la División de Atención al Contribuyente, en la Sección Consultas de la Intendencia Regional Lima y en la Gerencia de Centros de Servicios al Contribuyente (1997-2002).</li>
				    		<li>Jefe del Centro de Servicios al Contribuyente Callao – Supervisor Profesional de la División de Centros de Servicios (2002-2003).</li>
				    		<li>Jefe del Centro de Servicios al Contribuyente San Isidro – Supervisor Profesional de la División de Centros de Servicios (2003-2009).</li>
				    		<li>Coordinador en la Gerencia de Administración de Personal de la Intendencia Nacional de Recursos Humanos (2009-2011).</li>
				    		<li>Asesor Legal de la Gerencia de Dictámenes Tributarios (2012).</li>
				    		<li>Asesor Legal en la Gerencia de Programación y Gestión de Servicios al Contribuyente de la Intendencia Nacional de Servicios al Contribuyente (2011-2013).</li>
				    	</ul>

				    </div>
				    <div role="tabpanel" class="tab-pane space" id="settings">
				    	<ul>
				    		<li>Colegio de Abogados de Lima (CAL) – 1996</li>
				    		<li>Miembro del Instituto Peruano de Derecho Tributario (IPDT).</li>
				    	</ul>
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="univer">
				    	<ul>
				    		<li>Profesor de diversos cursos tributarios en el Instituto de Administración Tributaria (IAT) de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Profesor del Curso de Derecho Tributario en la Facultad de Derecho y Ciencias Políticas de la Universidad Inca Garcilaso de la Vega.</li>
				    		<li>Conferencista como representante de SUNAT en múltiples eventos organizados por diversas instituciones públicas y/o privadas, en Universidades, Cámaras de Comercio de Lima e Ica, Colegio de Abogados, Colegio de Contadores y organizaciones gremiales, entre otros.</li>
				    	</ul>
				    </div>
				  </div>

				</div>
            
            
            	<div class="col-lg-6 ">

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#perfil1" aria-controls="perfil1" role="tab" data-toggle="tab">Perfil</a></li>
				    <li role="presentation"><a href="#profile1" aria-controls="profile1" role="tab" data-toggle="tab">Especialización</a></li>
				    <li role="presentation"><a href="#messages1" aria-controls="messages1" role="tab" data-toggle="tab">Experiencia Profesional</a></li>
				    <li role="presentation"><a href="#settings1" aria-controls="settings1" role="tab" data-toggle="tab">Membresía</a></li>
				    <li role="presentation"><a href="#univer1" aria-controls="univer1" role="tab" data-toggle="tab">Enseñanza Universitaria</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="perfil1">
				    	<div class="space"></div>
				    	 <center><img src="img/team1a.jpg" class="img-responsive"></center>
				    	<div class="team-member-details">
                          <center>
                          	<h2>Roger F. Guisado Zuloaga</h2>
                          	<p>Abogado por la Universidad Particular San Martín de Porres</p>
                          </center>
                            <div class="team-member-socials">
                            	<center>
                                <a target="_blank" href="https://www.facebook.com/Tributa-Consultores-150700788429315/?fref=ts"><i class="fa team-member-icons fa-facebook"></i></a>
                                <a target="_blank" href="https://pe.linkedin.com/in/roger-f-guisado-zuloaga-0b16076b"><i class="fa team-member-icons fa-linkedin"></i></a>
                            </center>
                            </div>
                        </div>
            			
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="profile1">
				    	<ul>
				    		<li>Magister en Finanzas y Derecho Corporativo – ESAN (2011-2013).</li>
				    		<li>Curso de Administración Tributaria (CAT XV) – Instituto de Administración Tributaria y Aduanera – Superintendencia Nacional de Aduanas y Administración Tributaria (SUNAT).</li>
				    		<li>Especialización en Tributación por la Pontificia Universidad Católica del Perú.</li>
				    		<li>Especialización en Gestión Pública – Universidad del Pacífico.</li>
				    		<li>Especialización en Derecho de Arbitraje – Universidad de Lima.</li>
				    		<li>Desarrollo de Habilidades Gerenciales – CENTRUM.</li>
				    	</ul>
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="messages1">
				    	<ul>
				    		<li>Diciembre de 1993 hasta Marzo de 2012 en la Superintendencia Nacional de Aduanas y Administración Tributaria (19 años).</li>
				    		<li>Orientador Tributario en la División de Atención al Contribuyente (1994-1996).</li>
				    		<li>Profesional del Área de Absolución de Consultas (1996-1998).</li>
				    		<li>Auditor Resolutor del Departamento Jurídico (1998-2000).</li>
							<li>Auditor Resolutor de la División de Reclamos (2001-2003).</li>
							<li>Profesional de la Gerencia de Normas Tributarias (2003-2005).</li>
							<li>Supervisor de la División de Centros de Servicios al Contribuyente Santa Anita (2005-2008).</li>
							<li>Jefe de Sección VI de Servicios al Contribuyente (2009-2010).</li>
							<li>Jefe de Sección IV de Servicios al Contribuyente (2010-2011).</li>
				    	</ul>

				    </div>
				    <div role="tabpanel" class="tab-pane space" id="settings1">
				    	<ul>
				    		<li>Colegio de Abogados de Lima – 1994</li>
				    		<li>Miembro del Instituto Peruano de Derecho Tributario (IPDT).</li>
				    	</ul>
				    </div>
				    <div role="tabpanel" class="tab-pane space" id="univer1">
				    	<ul>
				    		<li>Pasante en Docencia del Instituto de Administración Tributaria de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Profesor de Tributación del Instituto de Administración Tributaria de la Superintendencia Nacional de Aduanas y Administración Tributaria – SUNAT.</li>
				    		<li>Profesor del Programa de Derecho Tributario del Centro Cultural de la Pontificia Universidad Católica del Perú.</li>
				    		<li>Expositor en los Encuentros Nacionales de la Intendencia Nacional Jurídica de la SUNAT.</li>
				    		<li>Conferencista en Universidades, Cámara de Comercio, Colegio de Contadores, entre otros gremios.</li>
				    	</ul>
				    </div>
				  </div>

				</div>
         </div>
    </div>

</section>

<?php
include('sub-footer.php');
include('footer.php');
?>