 <!-- Contactanos-->
    <section class="contact">
        <div class="col-lg-6 no-padding">
            <div style="background-image: url(img/service7.jpg);" class="imagen"></div>
        </div>
        <div class="col-lg-6 color-contact ">
            <div class=" text-center">
                <H3>SUSCRÍBETE A NUESTRO BOLETÍN</H3>
                <hr class="separator">
            </div>

            <div class="col-lg-12">
                <form action="" id="newsletter-form" novalidate="novalidate">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="nombre" name="nombre" type="text" class="form-control" placeholder="Nombre">
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="apellido" name="apellido" type="text" class="form-control" placeholder="Apellido">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                   <input id="email" name="email" type="email" class="form-control" placeholder="Correo"> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="terminos" name="terminos"> <a href="#" data-toggle="modal" data-target="#modalterminos">Acepto los términos y condiciones de uso</a> 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center;">
                            <button type="submit" class="btn boton-white">Enviar</button>
                        </div>
                    </form>
            </div>

            <div class="col-lg-12">
                <div class="space"></div>
                <p>Dirección : Calle Manco Segundo N° 2675 (Ex Los Nogales), Lince</p>
                <p>Teléfono : (01) 422 4646</p>
                <p>Email : consultas@tributaconsultores.com</p>

            </div>
            
        </div>
    </section>