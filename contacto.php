<?php
include ('head.php');
include('header.php');
?>
<section class="inner-banner">
	<img src="img/banner-g.jpg">
	<h2>Contactános</h2>
</section>

<!-- -->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 ">
                    <form>
					  <div class="form-group">
					    <label for="nombre">Nombres y Apellidos</label>
					    <input type="text" class="form-control" id="nombre" placeholder="">
					  </div>
					  <div class="form-group">
					    <label for="email">Email </label>
					    <input type="email" class="form-control" id="email" placeholder="">
					  </div>
					  <div class="form-group">
					    <label for="tel">Teléfono</label>
					    <input type="text" class="form-control" id="tel" placeholder="">
					  </div>
					  <div class="form-group">
					    <label for="servicio">Servicio</label>
					    <input type="text" class="form-control" id="servicio" placeholder="">
					  </div>
					  <div class="form-group">
					  	<label for="servicio">Mensaje</label>
					  	<textarea class="form-control" rows="3"></textarea>
					  </div>

					  <button type="submit" class="btn boton-firma">Solicitar Servicio</button>
					</form>

                             
                </div>

                <div class="col-lg-6 space">
                	<center><img src="img/logo.png" class=" img-responsive logo-firma" alt="" >
                		<div class="space"></div>
                		<p>SOMOS EXPERTOS TRIBUTARIOS</p>
                		<hr class="separator">
                		</center>
                		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15605.104709992547!2d-77.050298!3d-12.093236!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c857509aeb41%3A0x2965736dd281c720!2sCalle+Los+Nogales+2675%2C+Lince+15073%2C+Per%C3%BA!5e0!3m2!1ses!2spe!4v1499449021415" width="100%" height="250px" frameborder="0" style="border:0" allowfullscreen></iframe>
                		<div class="space"></div>
                		<p><i class="fa fa-map-marker" aria-hidden="true"></i> Calle Manco Segundo N° 2675 (Ex Los Nogales), Lince</p>
                		<p><i class="fa fa-phone" aria-hidden="true"></i> (01) 422 4646</p>
                		
                		<p><i class="fa fa-envelope-o" aria-hidden="true"></i> consultas@tributaconsultores.com</p>
                   
                </div>
            </div>
        </div>
        
    </section>

<?php

include('footer.php');
?>