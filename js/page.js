/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */
var f = new Date();
$(".year").text(f.getFullYear());

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('body').on('click', '.page-scroll a', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});


// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top',
    offset: $("nav").height() + 1
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});





$("li.pageScroll a").click(function(e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var offset = $(target).offset().top;
    var mainNav = $('.navbar-header');

    if(typeof $(".navbar-collapse").attr('aria-expanded') === 'undefined') {
        mainNav = $('nav');
    }
    offset -= mainNav.height() - 1;
    $('html, body').animate({
        scrollTop: offset
    }, 1000);
});

$(document).ready(function() {

    $('#carousel-example-generic').carousel({
        interval: 10000
    });
});

$(document).ready(function() {

    $('#carousel-generic').carousel({
        interval: 10000
    });

    $('#myTabs a').click(function (e) {
      e.preventDefault()
    $(this).tab('show')
})
});

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

