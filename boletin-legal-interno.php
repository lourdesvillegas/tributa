<?php
include ('head.php');
include('header.php');
?>

<section class="inner-banner">
  <img src="img/banner-g.jpg">
  <h2>Boletín Legal</h2>
</section>

<!-- -->
    <section class="body-int">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 space">
                  
                  <div class="blog-image-container">
                    <img src="img/service7.jpg" alt="" style="width: 100%;">
                  </div>
                  <!-- contenido blog-->
                  <div class="blog-content-container clearfix">
                    <div class="blog-title blog-panel">
                      <div class="blog-post-date">
                          <span class="do-post-date">04</span>
                          <span class="do-post-month">Jul</span>
                        </div>
                      <h2>Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</h2>
                    </div>

                    <div class="blog-content blog-panel">
                      <p style="text-align: justify;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.</p>
                      <p style="text-align: justify;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.</p>
                      <p style="text-align: justify;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.</p>
                      
                      </div>
                  </div><!-- fin contenido blog-->
    
                </div>

                <div class="col-lg-4 ">
                  <?php
                  include('panel-reciente.php');
                  ?>
                </div>
            </div>
        </div>
        
    </section>

<?php

include('footer.php');
?>