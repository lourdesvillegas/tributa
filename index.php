<?php

include ('head.php');
include('header.php');
?>


    <!-- Banner -->
    <section>
        <div class="container_img banner"> 
            <div class="container">
                <div class="text_img">
                    <p class="lema">SOMOS EXPERTOS TRIBUTARIOS</p>
                    <p class="sublema">BRINDAMOS ATENCIÓN PERSONALIZADA</p>
                </div>
            </div>                                  
        </div>
    </section>
    <!-- Fin Banner -->


    <!-- Servicios Section -->
    <section class="block">
        <div class="container">
            <div class="">
                <div class=" col-md-offset-1 col-lg-9  text-center">
                    <div id="carousel-example-generic" class="carousel slide img-responsive" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                
                                <div class="carousel-caption">
                                   <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) </p>
                                </div>
                            </div>
                            <div class="item">
                                
                                <div class="carousel-caption">
                                   <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) </p>
                                </div>
                            </div>
                            <div class="item">
                                
                                <div class="carousel-caption">
                                   <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) </p>
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Servicios Section -->




    <!-- section firma Section -->
    <section id="firma">
        <div class="container">
            <div class="">
                <div class="col-lg-12 text-center">
                    <h2>NUESTRA FIRMA</h2>
                    <hr class="separator">

                    <center><img src="img/logo.png" class=" img-responsive logo-firma" alt="" ></center>

                    <p>Somos profesionales especializados en materia tributaria con muchos años de experiencia en el rubro, dispuesto a satisfacer las necesidades de los clientes con la mayor transparencia y confidencialidad. Nuestra firma tiene la satisfacción de haber colaborado eficazmente para que nuestros clientes tomen decisiones oportunas y, del mismo modo, haberlos asesorado a resolver problemas concretos que dificultaban su marcha empresarial.</p>

                </div>
                <div class="col-lg-12 text-center logo-firma">
                    <a class="btn boton-firma" href="#" role="button">VER MÁS</a>
                </div>
            </div>
        </div>
    </section>

    <!-- derecho tributario 2-->
    <section id="tributario" class="">
         <div class=" container-fluid ">
                <div class="col-lg-6 color1">
                    <h2>ASESORÍA TRIBUTARIA</h2>
                    <hr class="separator">
                    <p>Siendo conocedores que el sistema tributario de nuestro país es complejo, nuestro estudio nace bajo la concepción de ofrecer una completa asesoría en materia fiscal a los clientes, con la finalidad de facilitar la toma de sus decisiones de manera acertiva, habiendo para ello diseñado diversos productos, tales como: Consultas tributarias y estudios especiales, reclamación, apelación y defensa judicial, planeamiento tributario, absolución de esquelas y cartas inductivas, acompañamiento legal en fiscalizaciones, entre otros servicios.</p>
                    <div class="space"></div>

                    <center><a class="btn boton-white  " href="#" role="button">VER MÁS</a></center>
                </div>            
                <div class="col-lg-6 no-padding">
                     <div style="background-image: url(img/service14.jpg);" class="imagen">
                    </div>
                </div>
        </div>
        <div class="container-fluid  ">
                <div class="col-lg-6 no-padding">
                    <div style="background-image: url(img/service1.jpg);" class="imagen">
                    </div>
                </div>
                <div class="col-lg-6 color1">
                    <h2>ASESORÍA CORPORATIVA</h2>
                    <hr class="separator">
                    <p>Adicionalmente, a nuestros servicios especializados en tributación, brindamos consultoría complementaria en Derecho Comercial, lo que comprende constitución de empresas, elaboración de todo tipo de contratos y liquidación de empresas conforme a la ley de la materia y vía declaración de quiebra judicial. Asimismo, nos encargamos de ejercer labores de representación de empresas no domiciliadas en el país y la constitución de sucursales extranjeras.</p>
                </br>
                    <div class="space"></div>
                    <center><a class="btn  boton-white " href="#" role="button">VER MÁS</a></center>
                </div>
        </div>
        <!-- derecho tributario 3-->
        <div class="container-fluid  ">
           
                <div class="col-lg-6 color1" style="padding-bottom: 125px;">
                    <h2>AUDITORÍA PREVENTIVA TRIBUTARIA</h2>
                    <hr class="separator">
                    <p>Buscamos minimizar las contingencias tributarias, verificando antes de cualquier intervención de la autoridad tributaria, el correcto cumplimiento de las obligaciones de nuestros clientes, a fin de subsanar las omisiones o infracciones que se pudieran estar presentando.</p>
                    <div class="space"></div>
                    <center><a class="btn   boton-white" href="#" role="button">VER MÁS</a></center>
                </div>            
                <div class="col-lg-6 no-padding">
                    <div style="background-image: url(img/plant-image.jpg);" class="imagen">
                    </div>
                </div>
        </div>
    </section>



    <!-- seccon socios-->
    <section id="team-section">
        <div class="container-fluid">
            <div class="">
                <div class="col-lg-12 text-center">
                    <h2>NUESTROS SOCIOS</h2>
                    <hr class="separator">
                </div>

                <div class="col-lg-12 ">
                    <div class="col-md-offset-3 col-lg-3 col-xs-6">
                        <div class="team-member">
                            <img src="img/team2a.jpg" class="img-responsive">
                            <div class="team-member-details">
                                <a href="#"><h2>Alfredo A. Carella Feliziani</h2></a>
                                <div class="team-member-socials">
                                    <a target="_blank" href="https://www.facebook.com/Tributa-Consultores-150700788429315/?fref=ts"><i class="fa team-member-icons fa-facebook"></i></a>
                                    <a target="_blank" href="#"><i class="fa team-member-icons fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                     <div class="  col-lg-3 col-xs-6">
                        <div class="team-member">
                            <img src="img/team1a.jpg" class="img-responsive">
                            <div class="team-member-details">
                                <a href="http://tributaconsultores.com/socio/roger-f-guisado-zuloaga/"><h2>Roger F. Guisado Zuloaga</h2></a>
                                <div class="team-member-socials">
                                    <a target="_blank" href="https://www.facebook.com/Tributa-Consultores-150700788429315/?fref=ts"><i class="fa team-member-icons fa-facebook"></i></a>
                                    <a target="_blank" href="https://pe.linkedin.com/in/roger-f-guisado-zuloaga-0b16076b"><i class="fa team-member-icons fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>
    </section>


    <!-- clientes-->
    <section id="clientes" class="">
         <div class="container-fluid">
          
                <div class="col-lg-3 no-padding">
                     <div style="background-image: url(img/service7.jpg);" class="imagen">
                    </div>
                </div>
                <div class="col-lg-3 no-padding color-clientes ">
                    <h2>NUESTROS CLIENTES</h2>
                    <hr class="separator">
                </div>
                <div class="col-lg-6 ">
                    <div class="  col-lg-12 ">
                    <div id="carousel-generic" class="carousel slide img-responsive" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-generic" data-slide-to="4"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner " role="listbox">
                            <div class="item active ">
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-agroindustrial.jpg" class=" img-responsive" alt="" >
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-alimas.jpg" class=" img-responsive" alt="" >
                                </div>
                            </div>
                            <div class="item ">
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-aprenda.jpg" class=" img-responsive" alt="" >
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-arquimagen.jpg" class=" img-responsive" alt="" >
                                </div>
                            </div>
                            <div class="item ">
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-capeco.jpg" class=" img-responsive" alt="" >
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-capunay.jpg" class=" img-responsive" alt="" >
                                </div>
                            </div>
                            <div class="item ">
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-coviem.jpg" class=" img-responsive" alt="" >
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-dondewalter.jpg" class=" img-responsive" alt="" >
                                </div>
                            </div>
                            <div class="item ">
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-eci.jpg" class=" img-responsive" alt="" >
                                </div>
                                <div class="col-lg-6 col-xs-6">
                                    <img src="img/clientes/client-electro.jpg" class=" img-responsive" alt="" >
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div class="space"></div>
                </div>

                    <center><a class="btn boton-firma  " href="#" role="button">VER MÁS</a></center>

                </div> 
        </div>
    </section>

    <!-- BLOG-->
    <section id="blog">
        <div class="container">
            <div class="">
                <div class="col-lg-12 text-center">
                    <h2>BLOG</h2>
                    <hr class="separator">
                </div>

                <div >
    
        
        <!-- start-post-grids -->
      
                <div class="col-md-12 space">

                          <div class="col-md-4">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="http://pattygradoscoach.com/assets/uploads/19251065_10154424528317132_1220247864_n.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Para tener una buena relación con los demás, primero debes tenerla contigo                          </h3>
                                          <p >
                                              Lo que vemos en otro es un reflejo nuestro. Este reflejo también se ve en nuestras relaciones.      </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                          
                                      </div>
                                  </div>
                              </div>
                            </div>   
                            <div class="col-md-4">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="http://pattygradoscoach.com/assets/uploads/Image-1_1.jpg" class=" img-responsive" alt="">
                                          
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Silencio interno y conexión con uno mismo                          </h3>
                                          <p >
                                              Una forma de lograr desarrollar nuestro lado espiritual es darnos un tiempo para hacer silencio, tanto a nivel del habla como de la mente     </p>
                                          <a href="#" class="btn boton-firma btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>  

                            <div class="col-md-4">
                              <div class="blog-item">
                                  <div class="blog-item-wrapper">
                                      <div class="blog-item-img">
                                          <img src="http://pattygradoscoach.com/assets/uploads/IMG_9912.jpg" class=" img-responsive"  alt="">
                                        
                                      </div>
                                      <div class="blog-item-description clearfix">
                                          <h3 >
                                              Entre perfecciones y falsedades                          </h3>
                                          <p >
                                              Los juicios que existen en nuestra sociedad, como "debes comportarte así..."  pueden tener como consecuencia que    </p>
                                          <a href="#" class="btn boton-firma  btn-small pull-right">Leer más</a>
                                      </div>
                                  </div>
                              </div>
                            </div>   
               
                    </div>
     
        
                </div>


            </div>
        </div>
    </section>


   

 <?php
include('sub-footer.php');

include('footer.php');
?>
